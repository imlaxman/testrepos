package com.raistudies.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.raistudies.domain.User;

@Controller
public class UserListController1 {
	private List<User> userList = new ArrayList<User>(); 
	
	@RequestMapping(value="/AddUser1.htm",method=RequestMethod.GET)
	public String showForm(ModelMap model){
		User user = new User();
		model.addAttribute("user", user);
		return "AddUser";
	}
	
	@RequestMapping(value="/AddUser1.htm",method=RequestMethod.POST)
	public @ResponseBody String addUser(Model model, @Validated User user,BindingResult result,HttpServletRequest request,Errors errors ){
		String returnText;
		//,BindException errors, 
		/*if(user.getName().equals("") || user.getName().equals("Hi")){
			errors.rejectValue("name","cannot be empt");
			return "AddUser";
		}*/
		
		if(!result.hasErrors()){
			userList.add(user);
			returnText = "User has been added to the list. Total number of users are " + userList.size();
		}else{
			returnText = "Sorry, an error has occur. User has not been added to list.";
		}
		return returnText;
	}
	
	
	
	/*@RequestMapping(value="/checkUser.htm",method=RequestMethod.GET)
	public String showCheckUserForm(Model model, @ModelAttribute("user") User user){
		model.addAttribute(user);
		return "AddUser";
	}*/
	
	/*@RequestMapping(value="/checkUser.htm",method=RequestMethod.POST)
	public @ResponseBody String checkUser(@ModelAttribute(value="user") User user, BindingResult result ){
		String returnText;
		if(user.getName().equals("hi")){
			returnText = "Sorry, an error has occurred........";
			//result.addError("error");
		}
		
		
		if(!result.hasErrors()){
			userList.add(user);
			returnText = "User has been added to the list. Total number of users are " + userList.size();
		}else{
			returnText = "Sorry, an error has occur. User has not been added to list.";
		}
		return returnText;
	}
*/
	
	
	
	@RequestMapping(value="/ShowUsers1.htm")
	public String showUsers(ModelMap model){
		model.addAttribute("Users", userList);
		return "ShowUsers";
	}
}
