var marketoShortcodeHandler = {
	rand_string: function(length){
		var rand = "";
		while(rand.length<length){
		    v = Math.random()<0.5?32:0;
		    rand += String.fromCharCode(Math.round(Math.random()*((122-v)-(97-v))+(97-v)));
		}
		return rand;
	},
	init: function(){
		var params = "";
		var action = "marketo_shortcode_handler";
		if (window.XMLHttpRequest) {
	        http = new XMLHttpRequest();
	    } else {
	        //IE6, IE5
	        http = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    http.onreadystatechange = function() {
	        if (http.readyState === 4 && http.status === 200) {
	        	var response = JSON.parse(http.response);
	        	var shortcodes = JSON.parse(http.response).shortcodes;
	        	if(response.success){
	        		//go through all .mkto_shortcode
	        		for (var i=0; i<shortcodes.length;i++){
	        			var element = document.querySelector('.mkto_shortcode[data-id="'+shortcodes[i].id+'"]');
	        			element.innerHTML = shortcodes[i].value;
	        		}
	        		if(typeof shortcodes.marketo_async_shortcodes !== 'undefined'){
		            	//there is an async shortcode inside
		            	marketo_async_shortcodes = shortcodes.marketo_async_shortcodes;
		            	http.open('POST', marketo_ajax_url+"?rand="+this.rand_string(10), true);
		            	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		            	params = "action="+action+"&content="+JSON.stringify(marketo_async_shortcodes);
		            	http.send(params);
		            }
	        	}
	        }
	    };

	    http.open('POST', marketo_ajax_url+"?rand="+this.rand_string(10), true);
	    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	    params = "action="+action+"&content="+encodeURIComponent(JSON.stringify(marketo_async_shortcodes));
	    http.send(params);
	}
};
(function() {
	marketoShortcodeHandler.init();
})();
