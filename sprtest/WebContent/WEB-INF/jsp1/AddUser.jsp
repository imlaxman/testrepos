<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Users using ajax</title>
<script src="/sprtest/js/jquery.js"></script>
<script type="text/javascript">
function doAjaxPost() {  
	  // get the form values  
	  var name = $('#name').val();
	  var education = $('#education').val();
	   
	  $.ajax({  
	    type: "POST",  
	    url: "/sprtest/AddUser.htm",  
	    data: "name=" + name + "&education=" + education,  
	    success: function(response){  
	      // we have the response  
	      $('#info').html(response);
	      $('#name').val('');
	      $('#education').val('');
	    },  
	    error: function(e){  
	      alert('Error: ' + e);  
	    }  
	  });  
	}  
</script>
</head>
<body>
<h1>Add Users using Ajax ........</h1>
<form:form commandName="user">
	<table>
		<tr><td>Enter your name : </td><td> <form:input type="text" path="name"/>
		<form:errors path="name" cssClass="errors"/>
		<br/></td></tr>
		<tr><td>Education : </td><td> <form:input type="text" path="education"/><br/></td></tr>
		<tr><td colspan="2">
			<button name="submit" type="submit"  class="text mleft1 mtop12"  id="contact-submit" onclick="doAjaxPost()">Submit</button>
		<!-- <input type="button" value="Add Users" onclick="doAjaxPost()"> -->
		<br/></td></tr>
		<tr><td colspan="2"><div id="info" style="color: green;"></div></td></tr>
	</table>
	</form:form>
	<a href="/sprtest/ShowUsers.htm">Show All Users</a>
</body>
</html>